#include "TempestRPC.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define INT 250
#define ACK 251
#define NAK 252
#define ESC 253
#define STX 254
#define ETX 255

/// initialises a new Tempest RPC struct
/// \param databuffer this is the buffer uses to pass data in and out of RPC methods
void InitTempestRPC(struct TempestRPC_t* instance, TempestRPCMethod_t* rpcArray, uint8_t* databuffer)
{
	instance->private_State = rcpsIdle;
	instance->InterruptReason = 0;
	instance->MethodReady = false;
	instance->private_ESCReceived = false;
	instance->private_UpdateINT = false;
	instance->FunctionArray = rpcArray;
    instance->SendReceiveDataArray = databuffer;
}

/// This method is used to signal the remote instance that the interrupt state changed
/// \param instance a pointer to struct that defines the instance of the RPC.
void SignalInterruptChange(struct TempestRPC_t* instance)
{
	instance->private_UpdateINT = true;
}

/// This private method will handle the calling of the remote and build-in methods
/// \param instance a pointer to struct that defines the instance of the RPC.
static void _invokeRemoteMethods(struct TempestRPC_t *instance)
{
	//check if it is an user defined method or build-in method
	if(instance->private_LastMethodID > 5)
	{
		//calculate the position of the method in the method array
		uint8_t id = instance->private_LastMethodID - 6;
		instance->ReturnDataLength = instance->FunctionArray[id](instance->SendReceiveDataArray, instance->ParameterDataLength);
	}
	else
	{
		uint32_t intflags;
		switch (instance->private_LastMethodID) //These are the Build-In methods
		{
		case 1: //Ping
			instance->ReturnDataLength = 1; //This will transmit the last received Byte back
			break;
		case 2: //Get Firmware Version
			instance->SendReceiveDataArray[0] = instance->Versions.FirmwareVersionMajor;
			instance->SendReceiveDataArray[1] = instance->Versions.FirmwareVersionMinor;
			instance->ReturnDataLength = 2;
			break;
		case 3: //Get Board Info
			instance->SendReceiveDataArray[0] = instance->Versions.BoardID;
			instance->SendReceiveDataArray[1] = instance->Versions.BoardRevision;
			instance->ReturnDataLength = 2;
			break;
		case 4: //Get Interrupt Reason
			instance->SendReceiveDataArray[0] = instance->InterruptReason;
			instance->SendReceiveDataArray[1] = instance->InterruptReason >>  8;
			instance->SendReceiveDataArray[2] = instance->InterruptReason >> 16;
			instance->SendReceiveDataArray[3] = instance->InterruptReason >> 24;
			instance->ReturnDataLength = 4;
			break;
		case 5: //Clear Interrupt Reason
			intflags =  (instance->SendReceiveDataArray[3] << 24 ) |
			  			(instance->SendReceiveDataArray[2] << 16) |
						(instance->SendReceiveDataArray[1] << 8) |
						instance->SendReceiveDataArray[0];
			instance->InterruptReason = instance->InterruptReason & ~intflags;
            if (instance->InterruptStateUpdated)
            {
                instance->InterruptStateUpdated(instance);
            }
			instance->ReturnDataLength = 0;
			break;
		}
	}
}

/// This private method will transmit de return data of a function back to the Controller
/// \param instance a pointer to struct that defines the instance of the RPC.
static void _transmitReturnData(struct TempestRPC_t* instance)
{
	uint8_t buffer[16] = { STX, instance->private_LastMethodID};
	uint16_t cs = STX;
	cs += instance->private_LastMethodID;
	uint16_t datalength = 0;
	for (int i = 0; i < instance->ReturnDataLength; ++i)
	{
		uint8_t d = instance->SendReceiveDataArray[i];
		if (d > 249)
		{
			datalength += 2;
		}
		else
		{
			datalength++;
		}
	}
	if (datalength < 250)
	{
		cs += datalength;
		buffer[2] = datalength;
		instance->TransmitDataToBus(&buffer[0], 3);
	}
	else //transmit length as a 14 bit number (ESC HIGH7bits LOW7bits)
	{
		uint8_t highBits = (datalength >> 7) & 0b01111111;
		uint8_t lowbits = datalength & 0b01111111;
		buffer[2] = ESC;
		buffer[3] = highBits;
		buffer[4] = lowbits;
		cs += ESC;
		cs += highBits;
		cs += lowbits;
		instance->TransmitDataToBus(&buffer[0], 5);
	}

	uint8_t dataIndex = 0;
	for (int i = 0; i < instance->ReturnDataLength; ++i)
	{
		uint8_t d = instance->SendReceiveDataArray[i];
		if(d > 249)
		{
			cs += ESC;
			d -= 249;
			cs += d;
			buffer[dataIndex++] =  ESC;
			buffer[dataIndex++] = d;
		}
		else
		{
			cs += d;
			buffer[dataIndex++] = d;
		}
		if (dataIndex > 14)
		{
			instance->TransmitDataToBus(&buffer[0], dataIndex);
			dataIndex = 0;
		}
	}
	if (dataIndex > 0) //transmit remaining data
	{
		instance->TransmitDataToBus(&buffer[0], dataIndex);
	}
	//The checksum is calculated by adding all bytes of the datagram and then taking the modulus 250.
	buffer[0] = cs % 249;
	buffer[1] = ETX;
	instance->TransmitDataToBus(&buffer[0], 2);
}

/// This method is called by the main application when a Byte arrives via the UART or other device.
/// this will mostly be called from the UART int handler
/// \param instance a pointer to struct that defines the instance of the RPC.
/// \param data The received Data Byte.
void ReceiveDataFromBus(struct TempestRPC_t* instance, uint8_t receivedData)
{
	//Bus State machine
	if (receivedData == STX)
	{
        instance->private_dataReceiveIndex = 0;
		instance->private_State = rcpsSTXReceived;
		instance->private_checksum = STX;
		instance->private_ESCReceived = false;
		return;
	}
	//capture private_State
	enum RPCState_t state = instance->private_State;
	if (state == rcpsSTXReceived)
	{
		instance->private_LastMethodID = receivedData;
		instance->private_State = rcpsFunctionIDReceived;
		instance->private_checksum += receivedData;
		return;
	}
	if (state == rcpsFunctionIDReceived)
	{
		instance->ParameterDataLength = receivedData;
		instance->private_dataReceiveCounter = receivedData;
		instance->private_State = rcpsDataLengthReceived;
		instance->private_checksum += receivedData;
		return;
	}
	if (state == rcpsDataLengthReceived)
	{
		if (instance->private_dataReceiveCounter > 0)
		{
			instance->private_checksum += receivedData;
			if (receivedData == ESC)
			{
				instance->private_ESCReceived = true;
                instance->private_dataReceiveCounter--;
				return;
			}
			if(instance->private_ESCReceived)
			{
				instance->private_ESCReceived = false;
				receivedData += 249; //when escaped the sender subtracts 249
			}
			instance->SendReceiveDataArray[instance->private_dataReceiveIndex++] = receivedData;
			instance->private_dataReceiveCounter--;
			return;
		}
		else
		{
			instance->private_LastReceivedChecksum = receivedData;
			instance->private_State = rcpsChecksumReceived;
			return;
		}
	}
	if (state == rcpsChecksumReceived)
	{
		if (receivedData == ETX)
		{
			instance->MethodReady = true;
		}
		instance->private_State = rcpsIdle;
		return;
	}
}


/// This method should be called from the main loop and will fire the remotely called method and functions
/// \param instance a pointer to struct that defines the instance of the RPC.
void handleRPCBus(struct TempestRPC_t* instance)
{
	if(instance->MethodReady)
	{
		instance->MethodReady = false;
		//The checksum is calculated by adding all bytes of the datagram and then taking the modulus 249.
		uint8_t cs = (uint8_t)(instance->private_checksum % 249);
		if (cs == instance->private_LastReceivedChecksum)
		{
			//invoke method
			_invokeRemoteMethods(instance);
			uint8_t transmitData = ACK;
			instance->TransmitDataToBus(&transmitData, 1);
			if (instance->ReturnDataLength > 0)
			{
				_transmitReturnData(instance);
			}
		}
		else
		{
			uint8_t transmitData = NAK;
			instance->TransmitDataToBus(&transmitData, 1);
		}
	}
	else
	{
		if(instance->private_UpdateINT)
		{
			instance->private_UpdateINT = false;
			uint8_t transmitData = INT;
			instance->TransmitDataToBus(&transmitData, 1);
		}
	}
}