
#ifndef TEMPESTRPCENDPOINT_LIBRARY_H
#define TEMPESTRPCENDPOINT_LIBRARY_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "LL_uart.h"

#ifdef __cplusplus
extern "C"
{
#endif

/// This is the function pointer definition for the remote functions
typedef uint16_t (* TempestRPCMethod_t)(uint8_t* data, size_t length);

enum RPCState_t
{
	rcpsIdle = 0,
	rcpsSTXReceived = 1,
	rcpsFunctionIDReceived = 2,
	rcpsDataLengthReceived = 3,
	rcpsDataReceived = 4,
	rcpsChecksumReceived = 5
};

struct Verion_t
{
	uint8_t FirmwareVersionMajor;
	uint8_t FirmwareVersionMinor;
	uint8_t BoardID;
	uint8_t BoardRevision;
};

struct TempestRPC_t;

typedef void (* InterruptStateUpdateMethod_t)(struct TempestRPC_t* instance);

struct TempestRPC_t
{
	enum RPCState_t private_State;
	uint8_t private_LastMethodID;
	uint8_t private_LastReceivedChecksum;
	uint16_t private_checksum;
	uint8_t private_dataReceiveCounter;
	uint8_t private_dataReceiveIndex;
	bool private_ESCReceived;
	volatile bool private_UpdateINT;

	/// this field signals the main method that it needs to handle the received data for executing a function
	volatile bool MethodReady;
	/// This field defines the number of bytes that the framework should send back to the Controller after a function call
	uint16_t ReturnDataLength;
	/// This field defines the number of bytes to be passed into the called function as parameters
	uint8_t ParameterDataLength;
	/// This field is a databuffer array that hold the received parameter data and also the function return data
	uint8_t* SendReceiveDataArray;
	/// This field contains the flags that define the interrupts
	uint32_t InterruptReason;
	struct Verion_t Versions;
	/// points to a function to handle the RS-232 writes
	LL_UART_WriteMethod_t TransmitDataToBus;
	InterruptStateUpdateMethod_t InterruptStateUpdated;
	TempestRPCMethod_t *FunctionArray;
};

/// This method is called by the main application when a Byte arrives via the UART or other device.
/// this will mostly be called from the UART int handler
/// \param instance a pointer to struct that defines the instance of the RPC.
/// \param data The received Data Byte.
void ReceiveDataFromBus(struct TempestRPC_t* instance, uint8_t data);

/// initialises a new Tempest RPC struct
/// \param databuffer this is the buffer uses to pass data in and out of RPC methods
void InitTempestRPC(struct TempestRPC_t* instance, TempestRPCMethod_t* rpcArray, uint8_t* databuffer);

/// This method should be called from the main loop and will fire the remotely called method and functions
/// \param instance a pointer to struct that defines the instance of the RPC.
void handleRPCBus(struct TempestRPC_t* instance);

/// This method is used to signal the remote instance that the interrupt state changed
/// \param instance a pointer to struct that defines the instance of the RPC.
void SignalInterruptChange(struct TempestRPC_t* instance);

#ifdef __cplusplus
}
#endif


#endif //TEMPESTRPCENDPOINT_LIBRARY_H
