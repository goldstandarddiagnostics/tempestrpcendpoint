#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

extern "C"
{
#include "init_rpc.h"
};

#define INT 250
#define ACK 251
#define NAK 252
#define ESC 253
#define STX 254
#define ETX 255

TEST_CASE("Statemachine changes" "[StateMachine]")
{
	GIVEN("A new Tempest RPC protocol struct")
	{
		struct TempestRPC_t rpcObject;
		struct TempestRPC_t* rpc = &rpcObject;
		uint8_t buffer[100];
		TempestRPCMethod_t rpcFunctionArray[11];
		InitTempestRPC(rpc,  &rpcFunctionArray[0], &buffer[0]);
		REQUIRE(rpc->private_State == rcpsIdle);
		WHEN("STX is received")
		{
			ReceiveDataFromBus(rpc, STX);
			handleRPCBus(rpc);
			REQUIRE(rpc->private_State == rcpsSTXReceived);
			THEN("Function ID 11 is received")
			{
				ReceiveDataFromBus(rpc, 11); //function ID = 11
				handleRPCBus(rpc);
				REQUIRE(rpc->private_State == rcpsFunctionIDReceived);
				REQUIRE(rpc->private_LastMethodID == 11);
				THEN("Parameter length 1 is received")
				{
					ReceiveDataFromBus(rpc, 1); //data length = 1
					handleRPCBus(rpc);
					REQUIRE(rpc->private_State == rcpsDataLengthReceived);
					REQUIRE(rpc->ParameterDataLength == 1);
				}
			}
		}
	}
}

uint8_t datatransmitted[255];
uint8_t datatransmissionCounter;
void testDataTransmitted(uint8_t* data, size_t length)
{
	memcpy(&datatransmitted[datatransmissionCounter], data, length);
	datatransmissionCounter += length;
}

bool passedTestMethodA = false;
uint16_t testMethodA(uint8_t* data, size_t length)
{
	passedTestMethodA = true;
	return 0;
}

TEST_CASE("method received no parameters" "[method receiving]")
{
	GIVEN("A new Tempest RPC protocol struct")
	{
		datatransmissionCounter = 0;
		struct TempestRPC_t rpcObject;
		struct TempestRPC_t* rpc = &rpcObject;
		uint8_t buffer[100];
		TempestRPCMethod_t rpcFunctionArray[6];
		InitTempestRPC(rpc, &rpcFunctionArray[0], &buffer[0]);
		rpc->TransmitDataToBus = testDataTransmitted;
		rpc->FunctionArray[0] = testMethodA;
		REQUIRE(rpc->private_State == rcpsIdle);
		WHEN("data is received")
		{
			ReceiveDataFromBus(rpc, STX);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, 6);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, 0);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, 11);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, ETX);
			handleRPCBus(rpc);
			REQUIRE(datatransmitted[0] == ACK);
			REQUIRE(passedTestMethodA == true);
		}
	}
}

uint16_t testMethodB(uint8_t* data, size_t length)
{
	REQUIRE(*data == 253);
	return 0;
}

TEST_CASE("method received 1 parameters and escaped data" "[method receiving]")
{
	GIVEN("A new Tempest RPC protocol struct")
	{
		datatransmissionCounter = 0;
		struct TempestRPC_t rpcObject;
		struct TempestRPC_t* rpc = &rpcObject;
		uint8_t buffer[100];
		TempestRPCMethod_t rpcFunctionArray[6];
		InitTempestRPC(rpc, &rpcFunctionArray[0], &buffer[0]);
		rpc->TransmitDataToBus = testDataTransmitted;
		rpc->FunctionArray[0] = testMethodB;
		REQUIRE(rpc->private_State == rcpsIdle);
		WHEN("data is received")
		{
			ReceiveDataFromBus(rpc, STX);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, 6);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, 2);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, ESC);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, 4);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, 21);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, ETX);
			handleRPCBus(rpc);
			REQUIRE(datatransmitted[0] == ACK);
		}
	}
}

uint16_t testMethodC(uint8_t* data, size_t length)
{
	REQUIRE(*data == 253);
	*data = 252;
	return 1;
}

TEST_CASE("method received 1 parameters and escaped data and returns escsaped data" "[method receiving]")
{
	GIVEN("A new Tempest RPC protocol struct")
	{
		datatransmissionCounter = 0;
		struct TempestRPC_t rpcObject;
		struct TempestRPC_t* rpc = &rpcObject;
		uint8_t buffer[100];
		TempestRPCMethod_t rpcFunctionArray[6];
		InitTempestRPC(rpc, &rpcFunctionArray[0], &buffer[0]);
		rpc->TransmitDataToBus = testDataTransmitted;
		rpc->FunctionArray[0] = testMethodC;
		REQUIRE(rpc->private_State == rcpsIdle);
		WHEN("data is received")
		{
			ReceiveDataFromBus(rpc, STX);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, 6);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, 2);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, ESC);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, 4);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, 21);
			handleRPCBus(rpc);
			ReceiveDataFromBus(rpc, ETX);
			handleRPCBus(rpc);
			REQUIRE(datatransmitted[0] == ACK);
			REQUIRE(datatransmitted[1] == STX);
			REQUIRE(datatransmitted[2] == 6);
			REQUIRE(datatransmitted[3] == 2);
			REQUIRE(datatransmitted[4] == ESC);
			REQUIRE(datatransmitted[5] == 3);
			REQUIRE(datatransmitted[6] == 20);
			REQUIRE(datatransmitted[7] == ETX);
		}
	}
}